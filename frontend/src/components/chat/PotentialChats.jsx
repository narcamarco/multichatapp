import React, { useContext } from 'react';
import { ChatContext } from '../../context/ChatContext';
import { AuthContext } from '../../context/AuthContext';

const PotentialChats = () => {
  const { potentialChats, createChat, onlineUsers } = useContext(ChatContext);

  const { user } = useContext(AuthContext);

  return (
    <>
      <div className="all-users">
        {potentialChats.length !== 0 &&
          potentialChats.map((pUser, index) => {
            return (
              <div
                className="single-user"
                key={index}
                onClick={() => createChat(user._id, pUser._id)}
              >
                {pUser.name}
                <span
                  className={
                    onlineUsers?.some((user) => {
                      return user?.userId === pUser?._id;
                    })
                      ? 'user-online'
                      : null
                  }
                ></span>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default PotentialChats;
