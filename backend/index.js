require('dotenv').config();
const express = require('express');
const cors = require('cors');
const path = require('path');

const connectToDatabase = require('./db/database');
const userRoute = require('./routes/userRoute');
const chatRoute = require('./routes/chatRoute');
const messageRoute = require('./routes/messageRoute');
const corsOptions = require('./config/corsOptions');

const app = express();

app.use(express.json());
app.use(cors(corsOptions));

app.use(express.static(path.join(__dirname, '..', 'frontend', 'build')));

app.use('/api/users', userRoute);
app.use('/api/chats', chatRoute);
app.use('/api/messages', messageRoute);

app.get('*', (req, res) => {
  res.sendFile(
    path.resolve(__dirname, '..', 'frontend', 'build', 'index.html')
  );
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, (req, res) => {
  console.log(`Server is running on PORT: ${PORT}....`);
  connectToDatabase();
});
