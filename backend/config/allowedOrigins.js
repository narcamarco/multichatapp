const allowedOrigins = [
  'http://localhost:3000',
  'http://localhost:5000',
  'http://localhost:8000',
  'https://chatmernapp-m9v1.onrender.com',
  'https://chatserver-6z57.onrender.com',
];

module.exports = allowedOrigins;
