const mongoose = require('mongoose');

const connectDatabase = () => {
  mongoose
    .connect(process.env.ATLAS_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then((data) => {
      console.log(`mongodb connected with server: ${data.connection.host}`);
    })
    .catch((error) => {
      console.log(`MongoDB connection error: ${error.message}`);
    });
};

module.exports = connectDatabase;
