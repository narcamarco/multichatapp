const express = require('express');

const {
  registerUser,
  loginUser,
  findUser,
  getUsers,
} = require('../controllers/userController');
const router = express.Router();

router.get('/', getUsers);
router.get('/find/:userId', findUser);
router.post('/register', registerUser);
router.post('/login', loginUser);

module.exports = router;
